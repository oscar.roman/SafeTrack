import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  TarjetaObservacion = 
  [
    {
      Titulo: 'Acciones Iniciales',
      Lista: 
      [
        { Nombre: 'Ojos en la Tarea', SubNombre: '' },
        { Nombre: 'No Apresurarse', SubNombre: '' },
        { Nombre: 'Equilibrio, Tracción, Agarre', SubNombre: '' }
      ]
    },
    {
      Titulo: 'Linea de Fuego',
      Lista: 
      [
        {Nombre: 'Posición del Cuerpo',SubNombre: '(Caer, golpeado por,golpear a, pellizcos)'},
        {Nombre: 'EPP',SubNombre: '(obligatorio, adecuado, en buenas codiciones, usado correctamente)'},
        {Nombre: 'Pantallas, Barreras, Barandales',SubNombre: ''},
        { Nombre: 'Aislamiento', SubNombre: '' }
      ]
    },
    {
      Titulo: 'Mecánica Corporal (Ergonomia)',
      Lista: 
      [
        { Nombre: 'Levantar, Doblar, Torcer', SubNombre:'' },
        { Nombre: 'Movimientos Repetitivos', SubNombre: '' },
        { Nombre: 'Alcanzar, Estirar, Empujar', SubNombre: '(Fuerza Excesiva)' },
        { Nombre: 'De pie, Sentado, Cluclillas', SubNombre: '(Periodos largos)' },
        { Nombre: 'Cómodo', SubNombre: '(vs. posicion incomoda)' },
      ]
    },
    {
      Titulo: 'Procedimientos y Estándares',
      Lista: 
      [
        { Nombre: 'Actualizado, Entendido', SubNombre: '' },
        { Nombre: 'Implementado', SubNombre: '' },
        { Nombre: 'Orden', SubNombre: '(Limpieza general, almacén, acceso)' },
      ]
    },
    {
      Titulo: 'Herramientas y Equipo',
      Lista: 
      [
        { Nombre: 'Condición Segura', SubNombre: '(Inspección Previa)' },
        { Nombre: 'Correcto para la tarea', SubNombre: '' },
        { Nombre: 'Uso Seguro', SubNombre: '' },
      ]
    },
  ]
  constructor(public navCtrl: NavController) {

  }

}
